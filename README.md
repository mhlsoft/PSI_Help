# 关于PSI - 开源ERP

---

PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。

![](/assets/01.jpg)

使用手册文档版本号：202005301040